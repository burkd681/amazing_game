{
    "id": "75c3a823-329f-45d9-9a46-41efe902c771",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object0",
    "eventList": [
        {
            "id": "70c3ebeb-96f9-4055-ae57-ea96c70738c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "75c3a823-329f-45d9-9a46-41efe902c771"
        },
        {
            "id": "c3283ee4-8881-48b1-851f-ac129c2bead5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 5,
            "m_owner": "75c3a823-329f-45d9-9a46-41efe902c771"
        },
        {
            "id": "5083d3fd-1027-4afd-a690-9810ef0d46b7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 5,
            "m_owner": "75c3a823-329f-45d9-9a46-41efe902c771"
        },
        {
            "id": "690348f6-5113-4273-9ca4-7c11575db2e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 37,
            "eventtype": 5,
            "m_owner": "75c3a823-329f-45d9-9a46-41efe902c771"
        },
        {
            "id": "717baece-37fc-4840-b4be-f573939b9e3a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 39,
            "eventtype": 5,
            "m_owner": "75c3a823-329f-45d9-9a46-41efe902c771"
        },
        {
            "id": "2b430d4b-79f5-4b49-8ba9-26ea98a4a72b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 38,
            "eventtype": 5,
            "m_owner": "75c3a823-329f-45d9-9a46-41efe902c771"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "986bf49b-ecc0-448f-8f79-51a45c7d99f3",
    "visible": true
}