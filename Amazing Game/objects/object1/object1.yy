{
    "id": "51ea5863-57c3-46bb-bb85-fc8da4385c05",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object1",
    "eventList": [
        {
            "id": "d4bff45b-3567-4a75-bea7-2105455f966e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "51ea5863-57c3-46bb-bb85-fc8da4385c05"
        },
        {
            "id": "c4c14207-bc81-464e-a819-4fb9e62b7530",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "75c3a823-329f-45d9-9a46-41efe902c771",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "51ea5863-57c3-46bb-bb85-fc8da4385c05"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}