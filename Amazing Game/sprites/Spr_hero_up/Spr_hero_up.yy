{
    "id": "763399ed-acc7-4984-8d87-7e4e111dec01",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Spr_hero_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "74a42c3b-67c7-40eb-ab67-5b6b573a0aa2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "763399ed-acc7-4984-8d87-7e4e111dec01",
            "compositeImage": {
                "id": "973076ee-be0e-4106-9b36-2f4c598454d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74a42c3b-67c7-40eb-ab67-5b6b573a0aa2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "baf08176-0910-414e-b7db-2d99bfa0ea13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74a42c3b-67c7-40eb-ab67-5b6b573a0aa2",
                    "LayerId": "1fda27ad-7922-4d08-a402-03d7d40ced5c"
                }
            ]
        },
        {
            "id": "d229cf71-ba9f-479d-a3c1-3986ddc43a0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "763399ed-acc7-4984-8d87-7e4e111dec01",
            "compositeImage": {
                "id": "ba435da8-3ef8-4ac6-a501-1d0ea666652a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d229cf71-ba9f-479d-a3c1-3986ddc43a0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "996022c7-7527-4930-b2e8-99dcbd5c76aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d229cf71-ba9f-479d-a3c1-3986ddc43a0c",
                    "LayerId": "1fda27ad-7922-4d08-a402-03d7d40ced5c"
                }
            ]
        },
        {
            "id": "0d405022-917b-4e98-a7fa-5ace187d416e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "763399ed-acc7-4984-8d87-7e4e111dec01",
            "compositeImage": {
                "id": "c97be41b-7576-4512-951a-57cdcb11a939",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d405022-917b-4e98-a7fa-5ace187d416e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cef97a2-cf43-48bf-b7d3-bc26f746008c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d405022-917b-4e98-a7fa-5ace187d416e",
                    "LayerId": "1fda27ad-7922-4d08-a402-03d7d40ced5c"
                }
            ]
        },
        {
            "id": "799110d2-020b-49c2-97ac-f5d536f82357",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "763399ed-acc7-4984-8d87-7e4e111dec01",
            "compositeImage": {
                "id": "68bf3e2f-454d-4fd2-8d51-396a6a378189",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "799110d2-020b-49c2-97ac-f5d536f82357",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9985383a-f434-4176-87ad-d4c51e6bfa1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "799110d2-020b-49c2-97ac-f5d536f82357",
                    "LayerId": "1fda27ad-7922-4d08-a402-03d7d40ced5c"
                }
            ]
        },
        {
            "id": "53f2db50-035b-4804-a123-850363874090",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "763399ed-acc7-4984-8d87-7e4e111dec01",
            "compositeImage": {
                "id": "cb5b27e4-a4e7-4ba8-a1d4-f87428830c87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53f2db50-035b-4804-a123-850363874090",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f39c0123-2d8f-4934-99b2-6f6d14313ffd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53f2db50-035b-4804-a123-850363874090",
                    "LayerId": "1fda27ad-7922-4d08-a402-03d7d40ced5c"
                }
            ]
        },
        {
            "id": "dfe851f6-0d46-4f0b-a165-1da1b3384679",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "763399ed-acc7-4984-8d87-7e4e111dec01",
            "compositeImage": {
                "id": "22621be6-4a57-4107-8022-a93defc04f1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfe851f6-0d46-4f0b-a165-1da1b3384679",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15f72db8-2d35-49d1-8580-790619a2d081",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfe851f6-0d46-4f0b-a165-1da1b3384679",
                    "LayerId": "1fda27ad-7922-4d08-a402-03d7d40ced5c"
                }
            ]
        },
        {
            "id": "3ed19a7a-2b40-4678-8a9a-e512ca182ab7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "763399ed-acc7-4984-8d87-7e4e111dec01",
            "compositeImage": {
                "id": "1f6efcb0-db2f-4c08-ba5b-5330c632ed2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ed19a7a-2b40-4678-8a9a-e512ca182ab7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65f7f6f5-cebd-4a52-8ce5-b3451049eaad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ed19a7a-2b40-4678-8a9a-e512ca182ab7",
                    "LayerId": "1fda27ad-7922-4d08-a402-03d7d40ced5c"
                }
            ]
        },
        {
            "id": "11594500-0645-43d9-abdb-c7c99a8346fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "763399ed-acc7-4984-8d87-7e4e111dec01",
            "compositeImage": {
                "id": "088843fa-0228-4a3c-906e-55a19a979480",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11594500-0645-43d9-abdb-c7c99a8346fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e549232-d829-4d90-8ca5-21410dc5420e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11594500-0645-43d9-abdb-c7c99a8346fd",
                    "LayerId": "1fda27ad-7922-4d08-a402-03d7d40ced5c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1fda27ad-7922-4d08-a402-03d7d40ced5c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "763399ed-acc7-4984-8d87-7e4e111dec01",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 0
}