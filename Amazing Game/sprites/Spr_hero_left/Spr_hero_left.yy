{
    "id": "664e1454-3e51-4e4d-9437-988d0a01e16d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Spr_hero_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b05d77e3-7df7-4608-a35f-018349b4b785",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "664e1454-3e51-4e4d-9437-988d0a01e16d",
            "compositeImage": {
                "id": "0c804675-37c1-4144-9537-df6b2bcdd271",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b05d77e3-7df7-4608-a35f-018349b4b785",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67391457-ae1d-49c2-bb0a-a5d05528413f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b05d77e3-7df7-4608-a35f-018349b4b785",
                    "LayerId": "ba20cd9f-9ff1-40b1-b579-3be6e8c656d9"
                }
            ]
        },
        {
            "id": "79574694-75f6-426d-8f73-556f1b5fcffa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "664e1454-3e51-4e4d-9437-988d0a01e16d",
            "compositeImage": {
                "id": "d51de6ae-d38d-4170-a4ad-a6a9163b779f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79574694-75f6-426d-8f73-556f1b5fcffa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "783b7e96-00e0-4ee9-9b73-4cceffb78ba1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79574694-75f6-426d-8f73-556f1b5fcffa",
                    "LayerId": "ba20cd9f-9ff1-40b1-b579-3be6e8c656d9"
                }
            ]
        },
        {
            "id": "1769560e-c673-4d46-9cad-ded95319f0be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "664e1454-3e51-4e4d-9437-988d0a01e16d",
            "compositeImage": {
                "id": "df1b5800-e044-42d8-acfc-2cde7703dbe0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1769560e-c673-4d46-9cad-ded95319f0be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72bebf56-5e61-4331-a037-a63d7a875cc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1769560e-c673-4d46-9cad-ded95319f0be",
                    "LayerId": "ba20cd9f-9ff1-40b1-b579-3be6e8c656d9"
                }
            ]
        },
        {
            "id": "24fd2714-c45c-488e-af6e-755b7674e63d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "664e1454-3e51-4e4d-9437-988d0a01e16d",
            "compositeImage": {
                "id": "c8a4d836-51d9-4e2a-8e32-1e3c4f3a786c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24fd2714-c45c-488e-af6e-755b7674e63d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2107b957-acac-4234-85df-020c7bc99224",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24fd2714-c45c-488e-af6e-755b7674e63d",
                    "LayerId": "ba20cd9f-9ff1-40b1-b579-3be6e8c656d9"
                }
            ]
        },
        {
            "id": "c9bfc541-e435-44dd-a0ce-42f2365576e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "664e1454-3e51-4e4d-9437-988d0a01e16d",
            "compositeImage": {
                "id": "3e476c11-e07a-4bfa-bbfa-e11fd55204df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9bfc541-e435-44dd-a0ce-42f2365576e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bf8ce0e-d26e-4e82-8dd5-c63b3e1d6da8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9bfc541-e435-44dd-a0ce-42f2365576e5",
                    "LayerId": "ba20cd9f-9ff1-40b1-b579-3be6e8c656d9"
                }
            ]
        },
        {
            "id": "3ed25f5e-b457-497b-85da-fbd26498ba67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "664e1454-3e51-4e4d-9437-988d0a01e16d",
            "compositeImage": {
                "id": "5ab4cdd0-0db7-4697-b4a5-b07c05f20a9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ed25f5e-b457-497b-85da-fbd26498ba67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9875fa7-1033-414e-8311-967056254efe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ed25f5e-b457-497b-85da-fbd26498ba67",
                    "LayerId": "ba20cd9f-9ff1-40b1-b579-3be6e8c656d9"
                }
            ]
        },
        {
            "id": "939ea814-d74b-4832-9a9f-58f0444310da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "664e1454-3e51-4e4d-9437-988d0a01e16d",
            "compositeImage": {
                "id": "a365867c-3aff-4229-8b90-d5d764ae5e9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "939ea814-d74b-4832-9a9f-58f0444310da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a2532ea-4f78-4281-a6c8-42ec39b200cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "939ea814-d74b-4832-9a9f-58f0444310da",
                    "LayerId": "ba20cd9f-9ff1-40b1-b579-3be6e8c656d9"
                }
            ]
        },
        {
            "id": "a2e687a5-92e6-4965-92f3-1dfc884921c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "664e1454-3e51-4e4d-9437-988d0a01e16d",
            "compositeImage": {
                "id": "8368f3b2-ea4d-4967-b27d-d14466e8415c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2e687a5-92e6-4965-92f3-1dfc884921c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "844de2d8-e630-441e-b17d-cf7a3ecf7fa2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2e687a5-92e6-4965-92f3-1dfc884921c7",
                    "LayerId": "ba20cd9f-9ff1-40b1-b579-3be6e8c656d9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ba20cd9f-9ff1-40b1-b579-3be6e8c656d9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "664e1454-3e51-4e4d-9437-988d0a01e16d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 0
}