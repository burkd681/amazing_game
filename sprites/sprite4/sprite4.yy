{
    "id": "5ad2f129-4a30-4bdd-8346-b3ccea53658f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dbf6a35f-5d18-4f5d-a5df-eedbc40ccb72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ad2f129-4a30-4bdd-8346-b3ccea53658f",
            "compositeImage": {
                "id": "40527e0b-02bc-4d9f-ad2f-04b19ad7cd99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbf6a35f-5d18-4f5d-a5df-eedbc40ccb72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3a5a8a8-ad21-4ff2-a7c9-4131e0abc37d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbf6a35f-5d18-4f5d-a5df-eedbc40ccb72",
                    "LayerId": "edecb797-75ca-45df-ba01-da512a292148"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "edecb797-75ca-45df-ba01-da512a292148",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5ad2f129-4a30-4bdd-8346-b3ccea53658f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}