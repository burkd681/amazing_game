{
    "id": "ced1fbca-2866-4c2e-94cc-5fa8e2318b87",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 1,
    "bbox_right": 29,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "89fa1b1c-0461-4a08-808f-0524dd366ee2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ced1fbca-2866-4c2e-94cc-5fa8e2318b87",
            "compositeImage": {
                "id": "079c6f42-0c11-4d2c-b9f4-146047056e49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89fa1b1c-0461-4a08-808f-0524dd366ee2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20b21455-a9ab-4084-938c-42c6a498d258",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89fa1b1c-0461-4a08-808f-0524dd366ee2",
                    "LayerId": "f4271a0f-1988-4fee-a101-7741b56bc6fa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f4271a0f-1988-4fee-a101-7741b56bc6fa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ced1fbca-2866-4c2e-94cc-5fa8e2318b87",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 0
}