{
    "id": "9a0cbec3-caba-48b7-8f53-c8ed821ddbf7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite7",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9557eb44-b435-44d2-9c8f-3cd16be92eb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a0cbec3-caba-48b7-8f53-c8ed821ddbf7",
            "compositeImage": {
                "id": "84864a86-b3ff-4413-832c-f44deabb43ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9557eb44-b435-44d2-9c8f-3cd16be92eb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c10fee22-71cd-49a5-81aa-65e103e49967",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9557eb44-b435-44d2-9c8f-3cd16be92eb6",
                    "LayerId": "46f8f395-1901-41d2-abf8-a97cf73c2a05"
                }
            ]
        },
        {
            "id": "0e0bf7e4-d9ab-41b6-9b7a-2f43d3531ade",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a0cbec3-caba-48b7-8f53-c8ed821ddbf7",
            "compositeImage": {
                "id": "31528ef6-5f3b-4f71-8f69-6c599dbed1d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e0bf7e4-d9ab-41b6-9b7a-2f43d3531ade",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67d49a99-91bb-45b8-8ce9-ef92d79787dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e0bf7e4-d9ab-41b6-9b7a-2f43d3531ade",
                    "LayerId": "46f8f395-1901-41d2-abf8-a97cf73c2a05"
                }
            ]
        },
        {
            "id": "0b91c659-60c3-44e9-afbe-37d6d6bda214",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a0cbec3-caba-48b7-8f53-c8ed821ddbf7",
            "compositeImage": {
                "id": "086fe2cf-9fd4-4d90-96ee-989b55ed92e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b91c659-60c3-44e9-afbe-37d6d6bda214",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "244d678f-0f54-4536-8f02-2df5123729dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b91c659-60c3-44e9-afbe-37d6d6bda214",
                    "LayerId": "46f8f395-1901-41d2-abf8-a97cf73c2a05"
                }
            ]
        },
        {
            "id": "a4a1fe8d-5762-4524-aac7-94fa4ab48b32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a0cbec3-caba-48b7-8f53-c8ed821ddbf7",
            "compositeImage": {
                "id": "d01be774-2081-43de-9399-1303507eea51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4a1fe8d-5762-4524-aac7-94fa4ab48b32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67d065f9-6a41-478a-baac-917d2910be75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4a1fe8d-5762-4524-aac7-94fa4ab48b32",
                    "LayerId": "46f8f395-1901-41d2-abf8-a97cf73c2a05"
                }
            ]
        },
        {
            "id": "c6e4d47a-2d3a-4f26-8125-c5ddf7ba3fb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a0cbec3-caba-48b7-8f53-c8ed821ddbf7",
            "compositeImage": {
                "id": "8ef881cb-f946-45e5-a0b6-e72f3d700acb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6e4d47a-2d3a-4f26-8125-c5ddf7ba3fb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7411ea19-a71a-490f-9c86-c258a69d0640",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6e4d47a-2d3a-4f26-8125-c5ddf7ba3fb9",
                    "LayerId": "46f8f395-1901-41d2-abf8-a97cf73c2a05"
                }
            ]
        },
        {
            "id": "2b0c5b49-640c-4162-8688-c50f4e340505",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a0cbec3-caba-48b7-8f53-c8ed821ddbf7",
            "compositeImage": {
                "id": "d46d162c-a322-4b8e-9b17-e082d37a3c55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b0c5b49-640c-4162-8688-c50f4e340505",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf9a0e9e-0a10-45de-b159-b38797d11b7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b0c5b49-640c-4162-8688-c50f4e340505",
                    "LayerId": "46f8f395-1901-41d2-abf8-a97cf73c2a05"
                }
            ]
        },
        {
            "id": "89f40318-6e94-45b3-a483-332f1c0b3ee9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a0cbec3-caba-48b7-8f53-c8ed821ddbf7",
            "compositeImage": {
                "id": "fd123168-f8de-4ba9-9f1e-344ec4df7187",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89f40318-6e94-45b3-a483-332f1c0b3ee9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d234ea61-bcb9-404c-bb68-38554d3019ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89f40318-6e94-45b3-a483-332f1c0b3ee9",
                    "LayerId": "46f8f395-1901-41d2-abf8-a97cf73c2a05"
                }
            ]
        },
        {
            "id": "f0e2da68-35ca-4143-9020-4c97bfafae82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a0cbec3-caba-48b7-8f53-c8ed821ddbf7",
            "compositeImage": {
                "id": "b09823c3-b7b8-4f78-857c-a007c35c20ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0e2da68-35ca-4143-9020-4c97bfafae82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8c3d78a-971e-4bd1-8e90-d60a4893a85d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0e2da68-35ca-4143-9020-4c97bfafae82",
                    "LayerId": "46f8f395-1901-41d2-abf8-a97cf73c2a05"
                }
            ]
        },
        {
            "id": "d90d4a1e-a6bc-41b7-898a-777f22421c51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a0cbec3-caba-48b7-8f53-c8ed821ddbf7",
            "compositeImage": {
                "id": "0627ad5b-046d-4e7e-aed8-cfed111c0721",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d90d4a1e-a6bc-41b7-898a-777f22421c51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8198d93-3633-4de5-82e6-abde26a1e3a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d90d4a1e-a6bc-41b7-898a-777f22421c51",
                    "LayerId": "46f8f395-1901-41d2-abf8-a97cf73c2a05"
                }
            ]
        },
        {
            "id": "ccd39907-e8f2-4c6c-aabf-1cda43053523",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a0cbec3-caba-48b7-8f53-c8ed821ddbf7",
            "compositeImage": {
                "id": "119f06cb-b83a-4c67-bfb7-0942adf15f61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccd39907-e8f2-4c6c-aabf-1cda43053523",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b401d9e-c9e5-4206-9f6a-fa082e2be016",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccd39907-e8f2-4c6c-aabf-1cda43053523",
                    "LayerId": "46f8f395-1901-41d2-abf8-a97cf73c2a05"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "46f8f395-1901-41d2-abf8-a97cf73c2a05",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9a0cbec3-caba-48b7-8f53-c8ed821ddbf7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}