{
    "id": "b79dcc82-128e-4c0f-b49d-943b9363fb25",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "77f58ddc-a314-4e7c-91f4-d539022cb275",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b79dcc82-128e-4c0f-b49d-943b9363fb25",
            "compositeImage": {
                "id": "19c82ba7-a49e-42d0-b95b-33fe356568b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77f58ddc-a314-4e7c-91f4-d539022cb275",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d7cbbaf-5288-4713-906e-53b0991acd55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77f58ddc-a314-4e7c-91f4-d539022cb275",
                    "LayerId": "2aa54b6f-4bf0-4c92-8cbf-3c69d1b25259"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2aa54b6f-4bf0-4c92-8cbf-3c69d1b25259",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b79dcc82-128e-4c0f-b49d-943b9363fb25",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}