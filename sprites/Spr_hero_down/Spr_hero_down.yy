{
    "id": "986bf49b-ecc0-448f-8f79-51a45c7d99f3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Spr_hero_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 3,
    "bbox_right": 20,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7b80fb57-5b85-4d61-ad89-528c539eb50f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "986bf49b-ecc0-448f-8f79-51a45c7d99f3",
            "compositeImage": {
                "id": "1b17e2c5-f838-460a-8c3c-9fd947905cb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b80fb57-5b85-4d61-ad89-528c539eb50f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2810a24c-8cd4-40ee-b8df-169627951ce7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b80fb57-5b85-4d61-ad89-528c539eb50f",
                    "LayerId": "dc78d21a-c581-4651-9c51-3478eb863d03"
                }
            ]
        },
        {
            "id": "163614b6-6522-4cda-b836-500c9f2721b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "986bf49b-ecc0-448f-8f79-51a45c7d99f3",
            "compositeImage": {
                "id": "89e2adba-d645-4c0f-b8ed-91c275a256af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "163614b6-6522-4cda-b836-500c9f2721b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4f99e65-274e-4989-b51a-e12de2f8e82b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "163614b6-6522-4cda-b836-500c9f2721b1",
                    "LayerId": "dc78d21a-c581-4651-9c51-3478eb863d03"
                }
            ]
        },
        {
            "id": "3f828221-0812-41b7-a119-f72c4a81698d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "986bf49b-ecc0-448f-8f79-51a45c7d99f3",
            "compositeImage": {
                "id": "15662064-e47f-4555-8f9b-4d5cf7355c8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f828221-0812-41b7-a119-f72c4a81698d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ea64450-7d37-4d8a-ac85-677045d31a5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f828221-0812-41b7-a119-f72c4a81698d",
                    "LayerId": "dc78d21a-c581-4651-9c51-3478eb863d03"
                }
            ]
        },
        {
            "id": "d337d1af-1b5c-4495-a829-17d357e6f04c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "986bf49b-ecc0-448f-8f79-51a45c7d99f3",
            "compositeImage": {
                "id": "238d71c6-9fb9-477a-a948-d7ef6aff60d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d337d1af-1b5c-4495-a829-17d357e6f04c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f12c5468-25c4-4c5a-9358-2035e48fe91f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d337d1af-1b5c-4495-a829-17d357e6f04c",
                    "LayerId": "dc78d21a-c581-4651-9c51-3478eb863d03"
                }
            ]
        },
        {
            "id": "2f17fc60-cc9f-4c3c-9429-d3bd5376a9c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "986bf49b-ecc0-448f-8f79-51a45c7d99f3",
            "compositeImage": {
                "id": "b4b7832d-1dae-43ab-960b-16baa9afecc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f17fc60-cc9f-4c3c-9429-d3bd5376a9c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d03de54-13d1-49a6-93a6-a31adf083bfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f17fc60-cc9f-4c3c-9429-d3bd5376a9c3",
                    "LayerId": "dc78d21a-c581-4651-9c51-3478eb863d03"
                }
            ]
        },
        {
            "id": "c900f21a-aa2b-48d7-b627-7f9f2683dc06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "986bf49b-ecc0-448f-8f79-51a45c7d99f3",
            "compositeImage": {
                "id": "f1dd6611-ec3c-4417-ad45-b76cfd837bc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c900f21a-aa2b-48d7-b627-7f9f2683dc06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4cb56ff-c80a-4870-9ffe-65a7d2fe2c55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c900f21a-aa2b-48d7-b627-7f9f2683dc06",
                    "LayerId": "dc78d21a-c581-4651-9c51-3478eb863d03"
                }
            ]
        },
        {
            "id": "cd384d46-08af-4beb-b11f-d404118f57ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "986bf49b-ecc0-448f-8f79-51a45c7d99f3",
            "compositeImage": {
                "id": "bbb2bace-d113-42e4-99e8-36f5f910d22d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd384d46-08af-4beb-b11f-d404118f57ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54d932aa-136e-4f99-91b3-955d501df5fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd384d46-08af-4beb-b11f-d404118f57ac",
                    "LayerId": "dc78d21a-c581-4651-9c51-3478eb863d03"
                }
            ]
        },
        {
            "id": "82472dd9-0277-4de6-97b8-811a773b36ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "986bf49b-ecc0-448f-8f79-51a45c7d99f3",
            "compositeImage": {
                "id": "5227a281-34b0-4e26-bed7-cd4795ad13b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82472dd9-0277-4de6-97b8-811a773b36ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1c591ad-e10d-4fc5-9211-969368a0d136",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82472dd9-0277-4de6-97b8-811a773b36ac",
                    "LayerId": "dc78d21a-c581-4651-9c51-3478eb863d03"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "dc78d21a-c581-4651-9c51-3478eb863d03",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "986bf49b-ecc0-448f-8f79-51a45c7d99f3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 0
}