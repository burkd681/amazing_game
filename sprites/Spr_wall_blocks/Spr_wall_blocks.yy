{
    "id": "062eeead-dd9d-4b7f-b512-6bd87e38ab8c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Spr_wall_blocks",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f99df976-b3dc-4f5b-8b01-d6838645a6b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "062eeead-dd9d-4b7f-b512-6bd87e38ab8c",
            "compositeImage": {
                "id": "28fd01f3-7906-4bce-b128-fe308ca8df7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f99df976-b3dc-4f5b-8b01-d6838645a6b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9da659c-43b4-4101-91c0-34fa40d9444f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f99df976-b3dc-4f5b-8b01-d6838645a6b6",
                    "LayerId": "aeeaaf39-99f9-47b9-a812-a2bb8941ef82"
                }
            ]
        },
        {
            "id": "17e2d9b9-fbc7-496d-bf5e-e3965202fc3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "062eeead-dd9d-4b7f-b512-6bd87e38ab8c",
            "compositeImage": {
                "id": "d0ecc83a-ae4f-4f7c-b95b-fb8de48f37eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17e2d9b9-fbc7-496d-bf5e-e3965202fc3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef8a7fbe-4251-4eeb-b551-de20dff8ed5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17e2d9b9-fbc7-496d-bf5e-e3965202fc3a",
                    "LayerId": "aeeaaf39-99f9-47b9-a812-a2bb8941ef82"
                }
            ]
        },
        {
            "id": "b3919c88-177d-4d0d-a85c-999cf42fedff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "062eeead-dd9d-4b7f-b512-6bd87e38ab8c",
            "compositeImage": {
                "id": "6f6b90e0-6347-48c2-8438-43c3d48f149b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3919c88-177d-4d0d-a85c-999cf42fedff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08f2b61d-e2ec-49f4-8796-7581284ba3c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3919c88-177d-4d0d-a85c-999cf42fedff",
                    "LayerId": "aeeaaf39-99f9-47b9-a812-a2bb8941ef82"
                }
            ]
        },
        {
            "id": "0973071e-4ebe-444d-afe5-7f2d20a8eec9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "062eeead-dd9d-4b7f-b512-6bd87e38ab8c",
            "compositeImage": {
                "id": "ca2be2a6-8f67-4238-a786-0cc2cc5429c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0973071e-4ebe-444d-afe5-7f2d20a8eec9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7f4229d-8483-4c3f-8aa5-7b1c29a0c01e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0973071e-4ebe-444d-afe5-7f2d20a8eec9",
                    "LayerId": "aeeaaf39-99f9-47b9-a812-a2bb8941ef82"
                }
            ]
        },
        {
            "id": "b07230d2-2268-4dda-a433-be3edcc45e0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "062eeead-dd9d-4b7f-b512-6bd87e38ab8c",
            "compositeImage": {
                "id": "08efa0dc-5ee0-4532-a03d-4cc59a889ffe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b07230d2-2268-4dda-a433-be3edcc45e0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d7dfb50-bb14-47cb-b52e-2b4beb40e437",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b07230d2-2268-4dda-a433-be3edcc45e0d",
                    "LayerId": "aeeaaf39-99f9-47b9-a812-a2bb8941ef82"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "aeeaaf39-99f9-47b9-a812-a2bb8941ef82",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "062eeead-dd9d-4b7f-b512-6bd87e38ab8c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}