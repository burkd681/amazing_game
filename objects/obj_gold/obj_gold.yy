{
    "id": "11a9d926-721d-437f-a06b-d1900330d410",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gold",
    "eventList": [
        {
            "id": "8ab8eb3f-da40-4bb0-bbee-708b981b4ca2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "11a9d926-721d-437f-a06b-d1900330d410"
        },
        {
            "id": "617c2edb-0075-48c1-8603-381cd343b503",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "75c3a823-329f-45d9-9a46-41efe902c771",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "11a9d926-721d-437f-a06b-d1900330d410"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "ced1fbca-2866-4c2e-94cc-5fa8e2318b87",
    "visible": true
}