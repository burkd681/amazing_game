{
    "id": "5106da8a-47b0-48cd-a3fc-b5f03fd6b69a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object6",
    "eventList": [
        {
            "id": "93f704a3-cfc1-4194-b926-c06d5bc27ef1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5106da8a-47b0-48cd-a3fc-b5f03fd6b69a"
        },
        {
            "id": "7afe3816-ecbd-4059-90c1-467aa236b9a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "5106da8a-47b0-48cd-a3fc-b5f03fd6b69a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9a0cbec3-caba-48b7-8f53-c8ed821ddbf7",
    "visible": true
}