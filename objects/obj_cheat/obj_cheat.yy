{
    "id": "8752d4b5-dd38-48f5-9ded-c9fb1f3ec2d0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cheat",
    "eventList": [
        {
            "id": "edb18eda-8821-48dc-9696-5c0c4c383e82",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8752d4b5-dd38-48f5-9ded-c9fb1f3ec2d0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5ad2f129-4a30-4bdd-8346-b3ccea53658f",
    "visible": false
}