{
    "id": "f6a33103-5068-4e68-a5c4-b7dd7f731101",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_portal",
    "eventList": [
        {
            "id": "c54129b0-897c-43cf-bbc7-2aae98e6d450",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f6a33103-5068-4e68-a5c4-b7dd7f731101"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b79dcc82-128e-4c0f-b49d-943b9363fb25",
    "visible": true
}