{
    "id": "bf2c0a6e-ace3-467f-8653-491ad74e3cc4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_wall",
    "eventList": [
        {
            "id": "ff65ab6c-4abd-4c7c-bde1-189e6701ee86",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bf2c0a6e-ace3-467f-8653-491ad74e3cc4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "062eeead-dd9d-4b7f-b512-6bd87e38ab8c",
    "visible": true
}