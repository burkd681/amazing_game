{
    "id": "75c3a823-329f-45d9-9a46-41efe902c771",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hero",
    "eventList": [
        {
            "id": "70c3ebeb-96f9-4055-ae57-ea96c70738c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "75c3a823-329f-45d9-9a46-41efe902c771"
        },
        {
            "id": "c3283ee4-8881-48b1-851f-ac129c2bead5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 5,
            "m_owner": "75c3a823-329f-45d9-9a46-41efe902c771"
        },
        {
            "id": "5083d3fd-1027-4afd-a690-9810ef0d46b7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 5,
            "m_owner": "75c3a823-329f-45d9-9a46-41efe902c771"
        },
        {
            "id": "690348f6-5113-4273-9ca4-7c11575db2e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 37,
            "eventtype": 5,
            "m_owner": "75c3a823-329f-45d9-9a46-41efe902c771"
        },
        {
            "id": "717baece-37fc-4840-b4be-f573939b9e3a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 39,
            "eventtype": 5,
            "m_owner": "75c3a823-329f-45d9-9a46-41efe902c771"
        },
        {
            "id": "2b430d4b-79f5-4b49-8ba9-26ea98a4a72b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 38,
            "eventtype": 5,
            "m_owner": "75c3a823-329f-45d9-9a46-41efe902c771"
        },
        {
            "id": "5edbe380-e604-4ea2-a64d-43d1fe6d9d30",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "bf2c0a6e-ace3-467f-8653-491ad74e3cc4",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "75c3a823-329f-45d9-9a46-41efe902c771"
        },
        {
            "id": "20a86851-901f-4f1a-9b4c-57ea04325156",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "b2a4759a-fd43-4bfb-ad23-8d22a2c6c8b6",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "75c3a823-329f-45d9-9a46-41efe902c771"
        },
        {
            "id": "6ca8e3b1-23f0-4f21-9efe-f04ffaf0dd53",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 33,
            "eventtype": 9,
            "m_owner": "75c3a823-329f-45d9-9a46-41efe902c771"
        },
        {
            "id": "2c8d25ab-c60a-487c-b0cc-b24fdf277aad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 34,
            "eventtype": 9,
            "m_owner": "75c3a823-329f-45d9-9a46-41efe902c771"
        },
        {
            "id": "92d541e9-c8c4-45b8-ba5d-4b6f52c0c188",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "11a9d926-721d-437f-a06b-d1900330d410",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "75c3a823-329f-45d9-9a46-41efe902c771"
        },
        {
            "id": "5bc5eaa3-652c-49fd-8a0b-e9c065b888fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "8752d4b5-dd38-48f5-9ded-c9fb1f3ec2d0",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "75c3a823-329f-45d9-9a46-41efe902c771"
        },
        {
            "id": "9d0790d9-4da0-48ce-9f6d-3eb39e533eb8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "f6a33103-5068-4e68-a5c4-b7dd7f731101",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "75c3a823-329f-45d9-9a46-41efe902c771"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "986bf49b-ecc0-448f-8f79-51a45c7d99f3",
    "visible": true
}