/// @DnDAction : YoYo Games.Rooms.If_Last_Room
/// @DnDVersion : 1
/// @DnDHash : 65093D5E
/// @DnDArgument : "not" "1"
if(room != room_last)
{

}

/// @DnDAction : YoYo Games.Instance Variables.If_Score
/// @DnDVersion : 1
/// @DnDHash : 4C434200
/// @DnDArgument : "op" "4"
/// @DnDArgument : "value" "50"
if(!variable_instance_exists(id, "__dnd_score")) __dnd_score = 0;
if(__dnd_score >= 50)
{
	/// @DnDAction : YoYo Games.Rooms.Next_Room
	/// @DnDVersion : 1
	/// @DnDHash : 0A944C6E
	/// @DnDParent : 4C434200
	room_goto_next();
}